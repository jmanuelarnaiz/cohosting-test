import { NgModule } from '@angular/core';
// Modules
import { RouterModule, Routes } from '@angular/router';

// Guards
import { AuthGuardService } from './guards/auth-guard.service';

// Components
import { PagesComponent } from './pages/pages.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { ProgressComponent } from './pages/progress/progress.component';
import { Graphics1Component } from './pages/graphics1/graphics1.component';
import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';
import { GuestListComponent } from './pages/guest/guest-list/guest-list.component';
import { GuestDetailComponent } from './pages/guest/guest-detail/guest-detail.component';
import { AddGuestComponent } from './pages/guest/add-guest/add-guest.component';

const appRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: 'guests', component: GuestListComponent, canActivate: [AuthGuardService] },
      { path: 'guests/:id', component: GuestListComponent, canActivate: [AuthGuardService] },
      { path: 'guest-details/:id', component: GuestDetailComponent,  canActivate: [AuthGuardService] },
      { path: 'guest-new', component: AddGuestComponent, canActivate: [AuthGuardService] },
      { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService] },
      { path: 'progress', component: ProgressComponent },
      { path: 'graphics1', component: Graphics1Component },
      { path: '', redirectTo: '/login', pathMatch: 'full' },
    ]
  },
  { path: 'login', component: LoginComponent  },
  // { path: 'register', component: DashboardComponent}
  { path: '**', component: NopagefoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
