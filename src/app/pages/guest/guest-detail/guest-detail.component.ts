import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { GuestModel } from '../../../models/user/guest.model';
import { GuestsService } from '../../../services/guests.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-guest-detail',
  templateUrl: './guest-detail.component.html',
  styles: []
})
export class GuestDetailComponent implements OnInit {

  guest: GuestModel;

  constructor(  private route: ActivatedRoute,
    private router: Router,
    private service: GuestsService,
    private _location: Location) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.guest = this.service.findGuestById(Number(id));
  }

  backToGuestsList() {
    this._location.back();
  }

}
