import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GuestsService } from 'src/app/services/guests.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-guest',
  templateUrl: './add-guest.component.html',
  styles: []
})
export class AddGuestComponent {

  // Forms
  addGuestForm: FormGroup;

  constructor(private _formBuilder: FormBuilder, private _guestService: GuestsService, private route: Router) {
    this.addGuestForm = _formBuilder.group(
      {
        // TODO: Needed validations
        emailValue: ['', Validators.required],
        nameValue: ['', Validators.required]
      }
      );
  }

  submitForm() {
    const name = this.addGuestForm.get('nameValue').value;
    const email = this.addGuestForm.get('emailValue').value;

    this._guestService.addGuest(name, email);
    this.route.navigate(['/guests']);
  }
}
