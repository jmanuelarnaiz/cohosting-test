import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuestRoutingModule } from './guests-routing.module';
import { GuestListComponent } from './guest-list/guest-list.component';
import { GuestDetailComponent } from './guest-detail/guest-detail.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { GuestsService } from '../../services/guests.service';
import { AddGuestComponent } from './add-guest/add-guest.component';

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // GuestRoutingModule
  ],
  declarations: [
    GuestListComponent,
    GuestDetailComponent,
    AddGuestComponent
  ],
  providers: [GuestsService]
})
export class GuestsModule {}
