import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const guestRoutes: Routes = [
//   { path: 'guests',  component: GuestListComponent },
//   { path: 'guests/:id', component: GuestDetailComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(guestRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class GuestRoutingModule { }
