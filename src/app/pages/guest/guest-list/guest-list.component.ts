import { Component, PipeTransform } from '@angular/core';
import { GuestsService } from '../../../services/guests.service';
import { Router} from '@angular/router';

import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { GuestModel } from '../../../models/user/guest.model';


@Component({
  selector: 'app-guest-list',
  templateUrl: './guest-list.component.html',
  styleUrls: ['/guest-list.component.scss'],
  providers: [DecimalPipe]
})
export class GuestListComponent {

  guestList: GuestModel[];
  guestList$: Observable<GuestModel[]>;

  filter = new FormControl('');

  constructor(pipe: DecimalPipe, private _guestsService: GuestsService, private router: Router) {
    // Simulate retrieve guest list from back
    this.guestList = this._guestsService.retieveGuestList();
    this.guestList$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text))
    );
  }

  navigateToDetail(id: number) {
    this.router.navigate(['/guest-details', id]);
  }

  navigateToNewGuestForm() {
    this.router.navigate(['/guest-new']);
  }

  // Private methods
  private search(text: string): GuestModel[] {
    return this.guestList.filter(guest => {
      const term = text.toLowerCase();
      return guest.$name.toLowerCase().includes(term);
    });
  }
}
