// Angular components
import { Component, OnInit } from '@angular/core';

// Sweet alert
import swal from 'sweetalert2';

// Forms
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// Services
import { SessionService } from '../services/session.service';
import { AuthenticationService } from '../services/auth.service';

// Routing
import { Router, ActivatedRoute } from '@angular/router';

const TAG = 'Login Component: ';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['/login.component.scss']
})


export class LoginComponent implements OnInit {

  // Forms
  loginForm: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private _sessionService: SessionService,
    private _authService: AuthenticationService,
    private _router: Router) {
    this.loginForm = _formBuilder.group(
      {
        // TODO: Needed validations
        emailValue: ['', Validators.required],
        pwdValue: ['', Validators.required]
      }
      );
  }

  ngOnInit() {
  }

  submitForm() {
    const email = this.loginForm.get('emailValue').value;
    const pwd = this.loginForm.get('pwdValue').value;

    this._authService.getLogin(email, pwd).then( (user => {
      if (user) {
        console.log(TAG, 'Login is correct');
        this._sessionService.storeAccessToken(user);
        this._router.navigateByUrl('/guests');
      } else {
        this.showAlert();
      }
    }),
    error => {
      this.showError();
      console.log(TAG, error);
    });
  }

  // PRIVATE METHODS
  private showAlert() {
    swal({
      title: 'Credenciales incorrectas',
      text: 'Revise el email y password',
      type: 'error',
      confirmButtonText: 'Aceptar'
  });
}

private showError() {
  swal({
    title: 'Error',
    text: 'Se ha producido un error',
    type: 'error',
    confirmButtonText: 'Aceptar'
});
}
}
