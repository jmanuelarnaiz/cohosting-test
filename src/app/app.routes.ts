// // Modules
// import { RouterModule, Routes } from '@angular/router';

// // Guards
// import { AuthGuardService } from './guards/auth-guard.service';

// // Components
// import { PagesComponent } from './pages/pages.component';
// import { DashboardComponent } from './pages/dashboard/dashboard.component';
// import { LoginComponent } from './login/login.component';
// import { ProgressComponent } from './pages/progress/progress.component';
// import { Graphics1Component } from './pages/graphics1/graphics1.component';
// import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';

// const appRoutes: Routes = [
//   {
//     path: '',
//     component: PagesComponent,
//     children: [
//       { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService] },
//       { path: 'progress', component: ProgressComponent },
//       { path: 'graphics1', component: Graphics1Component },
//       { path: '', redirectTo: '/login', pathMatch: 'full' },
//     ]
//   },
//   { path: 'login', component: LoginComponent  },
//   // { path: 'register', component: DashboardComponent}
//   { path: '**', component: NopagefoundComponent }
// ];

// export const APP_ROUTES = RouterModule.forRoot(appRoutes, { useHash: true });
