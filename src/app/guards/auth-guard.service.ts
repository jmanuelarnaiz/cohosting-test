import { Injectable } from '@angular/core';
import { SessionService } from '../services/session.service';
import { Observable } from 'rxjs';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private _sessionService: SessionService, private _router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this._sessionService.isLogged()) {
        return true;
    }

    // Navigate to login page
    if (this._router.url !== '/login') {
      this._router.navigate(['/login']);
    }
    return false;
  }
}
