export const status_enum = {
    SEND: 'Enviada',
    ACTIVE: 'Activa',
    NOT_SEND: 'No enviada'
};

export class GuestModel {
    private id: number;
    private name: string;
    private email: string;
    private lastOperation: string;
    private status: string;

    constructor(name, email, lastOperation?, status?) {
        this.id = Math.floor(Math.random() * 200);
        this.name = name;
        this.email = email;
        this.lastOperation = lastOperation ? lastOperation : '01/01/2019';
        this.status = status ? status : status_enum.NOT_SEND;
    }

    // GETTERS

    public get $id(): number {
        return this.id;
    }
    public get $name(): string {
        return this.name;
    }

    public get $email(): string {
        return this.email;
    }

    public get $status(): string {
        return this.status;
    }

    public get $lastOperation(): string {
        return this.lastOperation;
    }


    // getName(): string {
    //     return this.name;
    // }

    // getEmail(): string {
    //     return this.email;
    // }

    // getLastOperation(): string {
    //     return this.lastOperation;
    // }

    // getStatus(): number {
    //     return this.status;
    // }
}
