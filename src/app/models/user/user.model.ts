export class UserModel {
    private email: string;
    private token: string;

    constructor(data?: any) {
        this.email = data.email;
        this.token = data.token;
    }

    // Getters
    public getEmail(): string {
        return this.email;
    }
    public getToken(): string {
        return this.token;
    }
}
