import { Injectable } from '@angular/core';
import { UserModel } from '../models/user/user.model';

@Injectable()
export class SessionService {

    // Tendríamos un archivo externo de constantes para almacenar los items

  /**
   * Saves access token in local storage
   * @param user User to save
   */
  storeAccessToken(user: UserModel) {
    localStorage.setItem('user_model', JSON.stringify(user));
  }

  isLogged(): boolean {
    const rawUser = JSON.parse(localStorage.getItem('user_model'));

    if (rawUser != null) {
        const user = new UserModel(rawUser);
        return true;
    } else {
        return false;
    }
  }

}
