import { Injectable } from '@angular/core';
import { GuestModel, status_enum } from '../models/user/guest.model';

@Injectable({
  providedIn: 'root'
})
export class GuestsService {

  private guestList: GuestModel[];
  constructor() {
    // Lista de invitados 'hardcodeada'
    this.guestList = [
      new GuestModel('Fahima Everett', 'feveret@cohosting.es', '25/08/2017', status_enum.SEND),
      new GuestModel('Teigan Herrera', 'therrera@cohosting.es', '12/03/2016', status_enum.NOT_SEND),
      new GuestModel('Christian Shepard', 'cshepard@cohosting.es', '19/02/2018', status_enum.ACTIVE),
      new GuestModel('Mayson Barker', 'mbarker@cohosting.es', '11/11/2018', status_enum.SEND)
    ];
   }

   retieveGuestList(): GuestModel[] {
     // Esta sería una llamada a backend para recuperar los datos
     return this.guestList;
   }

   findGuestById(id: number): GuestModel {
     return this.guestList.find( guest =>  guest.$id === id );
   }

   addGuest(name: string, email: string) {
     this.guestList.push(new GuestModel(name, email));
   }
}
