import { Injectable } from '@angular/core';
import { UserModel } from '../models/user/user.model';

@Injectable()
export class AuthenticationService {

    /**
     * Este método llamaría a su vez usando HTTP Client a Backend para comprobar
     * si las credenciales introducidas por el usuario, son correcta
     * @param email User email
     * @param pwd  User password
     */
    getLogin(email: string, pwd: string): Promise<UserModel> {
        const query = { email: email, password: pwd }; // Esto lo pasaríamos a la llamada del back
        const API_LOGIN = 'http://my.service/authToken'; // Tendríamos una lista con las constantes de las llamadas a la API

        const error = false;
        return new Promise((resolve, reject) => {
            setTimeout(() => {
              // Suponemos que la llamada de back no ha dado error
              if (error) {
                reject(error);
              } else {
                if (email === 'admin@admin.es' && pwd === 'cohosting') {
                    // Crear un usuario model y guardar el token en el local Storage
                    const dataFromBack = { email: email, token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0N'};
                    const user = new UserModel(dataFromBack);
                    resolve(user);
                } else {
                    resolve(null);
                }
              }
            }, 1000); // Para simular el tiempo de llamada a back
          });
      }
}
